#include <pthread.h>
#include <semaphore.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#define SHARED 0  /* process-sharing if !=0, thread-sharing if =0*/
#define BUF_SIZE 20
#define MAX_MOD 100000
#define NUM_ITER 200

void* Producer(void *no); /* Producer thread */
void* Consumer(void *no); /* Consumer thread */

sem_t empty;            /* empty: How many empty buffer slots */
sem_t full;             /* full: How many full buffer slots */
sem_t b;                /* b: binary, used as a mutex */

int g_data[BUF_SIZE];   /* shared finite buffer  */
int g_idx = 0;              /* index to next available slot in buffer, 
                           remember that globals are set to zero
                           according to C standard, so no init needed  */

int main(int argc, char *argv[]) {
	//Dont run if params arent provided
	if (argc < 2 ){
		printf("Please provide number of threads as a parameter");
		return 0;
	}

	int i;
	//Number of threads
	long num = 0;
	char* ptr;

	//Get number of threads from parameter
	if (argv[1]){
		num = strtol(argv[1], &ptr, 10);
	}

	//Check that it is not zero
	if (num == 0) {
		printf("Something went wrong");
		return 0;
	}
	//Thread indexes
	int *prodIndex;
	int *consIndex;


	//Allocate memory
	prodIndex = calloc ((int)num, sizeof (int));
	consIndex = calloc ((int)num, sizeof (int));
	
	//Initialize arrays
	for(i = 0; i < (int)num ; i++){
		prodIndex[i] = i;
		consIndex[i] = i;
	}

	pthread_t *prodThread;
	pthread_t *consThread;

	//Allocate threads in memeory
	prodThread = malloc(sizeof(pthread_t) * (int)num);
	consThread = malloc(sizeof(pthread_t) * (int)num);

	// Initialize the semaphores
	sem_init(&empty, SHARED, BUF_SIZE);
	sem_init(&full, SHARED, 0);
	sem_init(&b, SHARED, 1);

	// Create the threads
	printf("main started\n");

	//Create threads 
	for (i = 0; i < (int)num; i++){
		printf("Created %d",i);
		pthread_create(&consThread[i], NULL, Consumer, (void*)&consIndex[i]);
		pthread_create(&prodThread[i], NULL, Producer, (void*)&prodIndex[i]);
	}

	//Wait for them to finish and join them
	for(i = 0; i < (int)num; i++) {
		pthread_join(consThread[i], NULL);
		pthread_join(prodThread[i], NULL);
	}


	printf("main done\n");

	//Cleanup
	free(prodIndex);
	free(consIndex);
	free(prodThread);
	free(consThread);

	return 0;
}


void *Producer(void *no) {
	int i=0, j;
	
	//Create int pointer with thread index
	int *c;
    c = (int*)no;

	while(i < NUM_ITER) {
		// pretend to generate an item by a random wait
		usleep(random()%MAX_MOD);
		
		// Wait for at least one empty slot
		sem_wait(&empty);
		// Wait for exclusive access to the buffer
		sem_wait(&b);
		
		// Check if there is content there already. If so, print 
    // a warning and exit.
		if(g_data[g_idx] == 1) { 
			printf("Producer overwrites!, idx er %d\n",g_idx); 
			exit(0); 
		}
		
		// Fill buffer with "data" (ie: 1) and increase the index.
		g_data[g_idx]=1;
		g_idx++;
		
		// Print buffer status.
		j=0; printf("(Producer %d, idx is %d): ",*c,g_idx);
		while(j < g_idx) { j++; printf("="); } printf("\n");
		
		// Leave region with exlusive access
		sem_post(&b);
		// Increase the counter of full bufferslots.
		sem_post(&full);
		
		i++;		
	}

	return 0;
}


void *Consumer(void *no) {
	int i=0, j;

	//Create int pointer with thread index
	int *c;
    c = (int*)no;

	while(i < NUM_ITER) {
		// Wait a random amount of time, simulating consuming of an item.
		usleep(random()%MAX_MOD);
		
		// Wait for at least one slot to be full
		sem_wait(&full);
		// Wait for exclusive access to the buffers
		sem_wait(&b);
		
		// Checkt that the buffer actually contains some data 
    // at the current slot.
		if(g_data[g_idx-1] == 0) { 
			printf("Consumes nothing!, idx er %d\n",g_idx);
			exit(0);
		}
		
		// Remove the data from the buffer (ie: Set it to 0) 
		g_data[g_idx-1]=0;
		g_idx--;
		
		// Print the current buffer status
		j=0; printf("(Consumer %d, idx is %d): " ,*c ,g_idx);
		while(j < g_idx) { j++; printf("="); } printf("\n");
		
		// Leave region with exclusive access
		sem_post(&b);
		// Increase the counter of empty slots.
		sem_post(&empty);  	

		i++;
	}

	return 0;
}

