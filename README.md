# Obligatorisk oppgave 1 - C-programmering med prosesser, tråder og synkronisering

Denne oppgaven består av følgende laboppgaver fra kompendiet:

* 4.5.b (Lage nye prosesser og enkel synkronisering av disse)
* 5.6.a (Lage nye tråder og enkel semafor-synkronisering av disse)
* 5.6.b (Flere Producere og Consumere)
* 6.10.a (Dining philosophers)

SE OPPGAVETEKST I KOMPENDIET. HUSK Å REDIGER TEKSTEN NEDENFOR!

## Gruppemedlemmer

* Mats Eikeland Mollestad
* Stian Andersen Negård
* André Gyrud Gunhildberget

## Kvalitetssikring

Vi brukte **clang-tidy** for å kvalitetssikre koden. 

**clang-tidy** foreslo at vi burde bruke noe annet enn **atoi**. Etter å ha rettet opp i dette ved å endre til **strtol** fikk vi ingen feilmeldinger.
**clang-tidy** foreslo også at vi måtte bruke **free** for å forhindre memory leaks på **oppgave lab-56b**.


For å utføre eksakt samme kvalitetssikring som vi har gjort, gjør følgende / kjør følgende kommandoer: **clang-tidy -checks="*"**
